import React, { useState } from 'react';
import { AiOutlineClose } from 'react-icons/ai';
import { HiOutlineMenuAlt4 } from 'react-icons/hi';
import Logo from '../assets/logo.png';



const Navbar = () => {
  const [nav, setNav] = useState(false);
  const [logo, setLogo] = useState(false)
  const handleNav = () => {
    setNav(!nav);
    setLogo(!logo)
  };
  const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div className='flex w-full justify-between items-center h-20 px-4 absolute z-10 text-white '>
      <div className="pt-4">
      <img src={Logo} alt="Logo" className="h-40 mt-20"/>
      </div>
      <ul className='hidden md:flex cursor-pointer'>
        <li>Home</li>
        <li className='nav-item hover:text-gray-300 cursor-pointer' onClick={handleClickScroll('inspirations')}>
          Inspirations
          </li>
        <li className='nav-item hover:text-gray-300 cursor-pointer' onClick={handleClickScroll('consultations')}>
          Consultations
          </li>
        <li className='nav-item hover:text-gray-300 cursor-pointer' onClick={handleClickScroll('gallery')}>
          Gallery
          </li>
        <li className='nav-item hover:text-gray-300 cursor-pointer' onClick={handleClickScroll('contact')}>
          Contact Us
          </li>
      </ul>
   

      {/* Hamburger */}
      <div onClick={handleNav} className='md:hidden z-10'>
        {nav ? <AiOutlineClose className='text-black' size={20} /> : <HiOutlineMenuAlt4 size={20} />}
      </div>

      {/* Mobile menu dropdown */}
      <div onClick={handleNav} className={nav ? 'absolute text-black left-0 top-0 w-full bg-white px-4 py-7 flex flex-col' : 'absolute left-[-100%]'}>
        <ul>
          <img src={Logo} alt="Logo" className="h-20 mt-20"/>
          <li className='border-b'>Home</li>
          <li onClick={handleClickScroll('inspirations')} 
          className='border-b'>
            Inspirations
            </li>
          <li onClick={handleClickScroll('consultations')}
           className='border-b'>
            Consultation
            </li>
          <li onClick={handleClickScroll('gallery')}
          className='border-b'>
            Gallery
            </li>
          <li onClick={handleClickScroll('contact')} 
          className='border-b'>
            Contact Us</li>
          <div className='flex flex-col'>
          </div>
        </ul>
      </div>
    </div>
  );
};

export default Navbar;
