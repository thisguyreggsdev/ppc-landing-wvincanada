import React from 'react';

const Search = () => {
  return (
    <div className='max-w-[1240px] mx-auto'>
      <div className='lg:col-span-2 flex flex-col justify-evenly'>
        <div>
          <h2 className='text-center text-3xl'>Transform your home or office into a work of  <span className='text-indigo-500'>art. </span></h2>
          <p className='py-4 text-lg text-justify '>
          At WVIN, we pride ourselves on striking the perfect balance between functionality and beauty, earning a reputation for delivering exceptional interior design results. Our unique approach combines a pragmatic sensibility with an aristocratic flair that sets us apart in the competitive world of interior design.

          Based in Canada, our dynamic and youthful team specializes in a wide range of interior design projects, including residential and commercial spaces, as well as renovation and remodeling projects. Our commitment to innovative solutions and exceptional client service ensures that every project is tailored to meet the specific needs and aspirations of our clients.
          </p>
          <p className='py-4 text-lg text-justify'>
            Whether you're looking to create a cozy living room or a stylish office space, our team has the expertise and creativity to bring your vision to life. From concept development to the final touches, we'll work closely with you every step of the way to ensure that your space not only functions efficiently but also exudes an undeniable charm and allure. Trust WVIN to transform your space into the interior of your dreams.
          </p>
        </div>
        <div className='mx-auto grid sm:grid-cols-2 gap-8 py-4'>
          <div className='flex flex-col lg:flex-row items-center text-center mt-8'>
            <div>
                <h3 className='py-2 text-3xl'>Our Values</h3>
                <p className='py-1 text-lg text-justify'>Our commitment to innovation means that we are always pushing the boundaries of design, seeking out new materials, technologies, and techniques to create truly unique and stunning spaces. And with a focus on nurturing the success of both our clients and our team members, we strive to create a positive and fulfilling experience for everyone involved.</p>
            </div>
          </div>
          <div className='flex flex-col lg:flex-row items-center text-center mt-8'>
            <div>
                <h3 className='py-2 text-3xl'>Our Goal</h3>
                <p className='py-1 text-lg text-justify'>We believe that building lasting relationships with our clients is key to our success. Our approach is centered around effective communication and collaboration, ensuring that every project is a true partnership. We are committed to positively impacting communities through our work. We believe that interior design has the power to spaces, and to leave a meaningful legacy every project we undertake. </p>
            </div>
          </div>
        </div>
      </div>

      <div>
          
          </div>
      </div>
    
  );
};

export default Search;
