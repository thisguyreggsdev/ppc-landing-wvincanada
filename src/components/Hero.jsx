import React from 'react';
import hero from '../assets/hero2.jpg';

const Hero = () => {
  const handleClickScroll = (sectionId) => () => {
    const section = document.getElementById(sectionId);
    if (section) {
      section.scrollIntoView({ behavior: 'smooth' });
    }
  };

  return (
    <div className='w-full h-screen relative'>
     <img
      src={hero}
      className='w-full h-full object-cover transform hover:translate-xy-2 transition duration-500'
        alt=''
        />
      <div className='absolute w-full h-full top-0 left-0 bg-gray-900/30'></div>
      <div className='absolute top-0 w-full h-full flex flex-col justify-center text-center text-white p-4'>
        <h2 className='py-4 text-4xl'>Design Your Dream Space with Ease </h2>
       <form className='flex justify-center items-center max-w-[700px] mx-auto w-full'>
       <p className='text-white font-bold py-2 px-4 rounded border-2 border-white rounded-8 cursor-pointer hover:bg-white hover:text-black transition duration-300 hover:shadow-md hover:scale-105' onClick={handleClickScroll('contact')}>
          Talk to us
        </p>
        </form>
      </div>
    </div>
  );
};

export default Hero;
