import React from 'react';
import Destinations from './components/Destinations';
import Footer from './components/Footer';
import Hero from './components/Hero';
import Navbar from './components/Navbar';
import Search from './components/Search';
import Selects from './components/Selects';
import Form from './components/Form';

function App() {
  return (
    <div>
      <Navbar />
      <Hero />
      <div id="inspirations">
      <Destinations />
      </div>
      <div id="consultations">
      <Search />
      </div>
      <div id="gallery">
      <Selects />
      </div>
      <div id="contact">
      <Form/>
      </div>
      <Footer />
    </div>
  );
}

export default App;
